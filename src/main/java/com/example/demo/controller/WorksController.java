package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WorksController {
    @GetMapping("/")
    public String home() {
	return "home";
    }

    @GetMapping("/works")
    public String works() {
	return "works";
    }

}
