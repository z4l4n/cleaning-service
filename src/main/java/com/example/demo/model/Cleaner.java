package com.example.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Entity
@Table(indexes = { @Index(name = "NAME_NDX", columnList = "NAME") })
public class Cleaner {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(name = "NAME")
    @Max(30)
    private String name;

    @NotNull
    private float hoursPerMonth;

    @NotNull
    private boolean male;

    @OneToMany(mappedBy = "assignedCleaner", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @Valid
    private List<Work> assignedWorks;

}
