package com.example.demo.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Table(indexes = { @Index(name = "DATE_NDX", columnList = "START_DATE") })
@Entity
public class Work {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(name = "START_DATE")
    private Date startDate;

    @NotNull
    private float estimatedDurationInHours;

    @Max(50)
    @NotNull
    private String location;

    @Max(30)
    @NotNull
    private String clientName;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "CLEANER_ID")
    @Valid
    private Cleaner assignedCleaner;

}
